#!/bin/zsh

setxkbmap dvp-it -option
setxkbmap dvp-it -option ctrl:swapcaps -option lv3:ralt_switch

sleep 2 && {
    # tab as modifier
    xmodmap -e "keycode 23 = Hyper_L"
    xmodmap -e "remove mod4 = Hyper_L"
    xmodmap -e "keycode any = Tab"
    sleep 0.5 && xcape -e "Hyper_L=Tab"

    # '@' as modifier (on us qwerty is the \)
    xmodmap -e "keycode 35 = Hyper_R"
    xmodmap -e "remove mod4 = Hyper_R"
    xmodmap -e "keycode any = at asciicircum"
    sleep 0.5 && xcape -e "Hyper_R=at"

    xcape -e "Control_L=Escape"
} &
